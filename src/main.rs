//! # bs
//!
//! bs (binary searcher) is a little tool to search patterns in binary data.
//! It can be useful to locate binary signatures.
//!
//! ## Basic example
//!
//! Search for PNG images in a tar file:
//! ```shell
//! $ bs -H '89 50 4E 47 0D 0A 1A 0A' /tmp/pictures.tar
//! /tmp/pictures.tar 89504e470d0a1a0a 0x121e00
//! /tmp/pictures.tar 89504e470d0a1a0a 0x127200
//! /tmp/pictures.tar 89504e470d0a1a0a 0x18e800
//! /tmp/pictures.tar 89504e470d0a1a0a 0x199200
//! /tmp/pictures.tar 89504e470d0a1a0a 0x1ad400
//! /tmp/pictures.tar 89504e470d0a1a0a 0x1e5c00
//! /tmp/pictures.tar 89504e470d0a1a0a 0x1ec000
//! /tmp/pictures.tar 89504e470d0a1a0a 0x1ed800
//! ```
//!
//! You can also use several patterns, and give them names for identification:
//! ```shell
//! $ bs -H JPG:ffd8,'PNG:89 50 4E 47 0D 0A 1A 0A' /tmp/pictures.tar
//! /tmp/pictures.tar JPG 0x200
//! /tmp/pictures.tar JPG 0xf6e00
//! /tmp/pictures.tar PNG 0x121e00
//! /tmp/pictures.tar PNG 0x127200
//! /tmp/pictures.tar JPG 0x13cfa1
//! /tmp/pictures.tar JPG 0x175786
//! /tmp/pictures.tar JPG 0x1887ae
//! /tmp/pictures.tar PNG 0x18e800
//! /tmp/pictures.tar PNG 0x199200
//! /tmp/pictures.tar JPG 0x1a3f7f
//! /tmp/pictures.tar PNG 0x1ad400
//! /tmp/pictures.tar JPG 0x1af19c
//! /tmp/pictures.tar JPG 0x1e5891
//! /tmp/pictures.tar PNG 0x1e5c00
//! /tmp/pictures.tar PNG 0x1ec000
//! /tmp/pictures.tar PNG 0x1ed800
//! /tmp/pictures.tar JPG 0x2028a8
//! /tmp/pictures.tar JPG 0x206e00
//! ```
//!
//! It seems that the JPG signature retrieves false positives. However since
//! the files are aligned you can search only in the positions aligned with 0x100:
//! ```shell
//! $ bs -H JPG:ffd8,'PNG:89 50 4E 47 0D 0A 1A 0A' -a 0x100 /tmp/pictures.tar
//! /tmp/pictures.tar JPG 0x200
//! /tmp/pictures.tar JPG 0xf6e00
//! /tmp/pictures.tar PNG 0x121e00
//! /tmp/pictures.tar PNG 0x127200
//! /tmp/pictures.tar PNG 0x18e800
//! /tmp/pictures.tar PNG 0x199200
//! /tmp/pictures.tar PNG 0x1ad400
//! /tmp/pictures.tar PNG 0x1e5c00
//! /tmp/pictures.tar PNG 0x1ec000
//! /tmp/pictures.tar PNG 0x1ed800
//! /tmp/pictures.tar JPG 0x206e00
//! ```
//!
//! Also, you can also use a file with the patterns:
//! ```shell
//! $ bs -c patterns/pictures.toml -a 0x100 /tmp/pictures.tar
//! ```
//! Besides, you can read from stdin:
//! ```shell
//! $ cat /tmp/pictures.tar | bs -H 'PNG:89 50 4E 47 0D 0A 1A 0A'
//! ```
//!
//!

mod args;
mod patterns;
mod utils;

use crate::args::Args;
use crate::patterns::Pattern;
use crate::utils::vec_to_hexdump;
use log::{debug, error, info};
use std::fs::File;
use std::io::{Read, BufReader, stdin};
use stderrlog;

fn main() {
    let args = Args::parse_args();
    init_log(args.verbosity);

    let searcher = Searcher::new(args.patterns, args.align);

    if args.filepaths.is_empty() {
        if let Err(err) = search_in_stdin(searcher) {
            error!("{}", err);
        }
    } else {
        for filepath in args.filepaths.iter() {
            if let Err(err) = search_in_file(&filepath, searcher.clone()) {
                error!("{}", err);
            }
        }
    }
}

fn init_log(verbosity: usize) {
    stderrlog::new()
        .module(module_path!())
        .verbosity(verbosity)
        .init()
        .expect("Error initiating log");
}

fn search_in_stdin(searcher: Searcher) -> Result<(), String> {
    return search_in("stdin", &mut stdin(), searcher);
}

fn search_in_file(filepath: &str, searcher: Searcher) -> Result<(), String> {
    let file = File::open(filepath)
        .map_err(|e| format!("Error opening file {}: {}", filepath, e))?;
    let mut file_reader = BufReader::with_capacity(0x100000, file);

    return search_in(filepath, &mut file_reader, searcher);
}

fn search_in(
    name: &str,
    reader: &mut dyn Read,
    mut searcher: Searcher,
) -> Result<(), String> {
    info!(
        "Looking in {} for {}",
        name,
        searcher
            .patterns
            .iter()
            .map(|p| vec_to_hexdump(&p.bytes))
            .collect::<Vec<String>>()
            .join(",")
    );

    let mut pos = 0;
    for byte in reader.bytes() {
        let byte = byte
            .map_err(|e| format!("Error reading {}:{} : {}", name, pos, e))?;

        for m in searcher.next_byte(pos, byte) {
            print_match(&name, &m);
        }
        pos += 1;

        if pos % 0x40000000 == 0 {
            info!("{}: 0x{:x} bytes read", name, pos);
        }
    }
    info!("Finish {}: {} bytes checked", name, pos);
    return Ok(());
}

fn print_match(filename: &str, m: &Match) {
    println!(
        "{} {} 0x{:x}",
        &filename, &m.pattern, m.start_position
    );
}


#[derive(Clone)]
struct Searcher {
    align: usize,
    patterns: Vec<Pattern>,
    pattern_checkers: Vec<PatternChecker>,
}

impl Searcher {
    pub fn new(patterns: Vec<Pattern>, align: usize) -> Self {
        return Self {
            align,
            patterns,
            pattern_checkers: Vec::new(),
        };
    }

    fn is_aligned(&self, pos: usize) -> bool {
        return pos % self.align == 0;
    }

    pub fn next_byte(&mut self, pos: usize, byte: u8) -> Vec<Match> {
        let mut new_checkers = Vec::new();

        // only start searching in aligned positions
        if self.is_aligned(pos) {
            for p in self.patterns.iter() {
                if p.is_first_byte(byte) {
                    debug!(
                        "byte {:x} at 0x{:x} begins pattern {}",
                        byte,
                        pos,
                        p.hexdump()
                    );
                    new_checkers.push(PatternChecker::new(p.clone(), pos));
                }
            }
        }

        for _ in 0..self.pattern_checkers.len() {
            let mut pc = self.pattern_checkers.pop().unwrap();
            if pc.is_next_byte(byte) {
                debug!("byte {:x} in pattern {}", byte, pc.pattern.hexdump());
                new_checkers.push(pc);
            }
        }

        let mut matches = Vec::new();
        new_checkers.retain(|pc| {
            if pc.is_finish() {
                matches.push(Match {
                    pattern: pc.pattern.clone(),
                    start_position: pc.start_position,
                });
                return false;
            }
            return true;
        });

        self.pattern_checkers = new_checkers;

        return matches;
    }
}

struct Match {
    pub pattern: Pattern,
    pub start_position: usize,
}

/// Keeps the state of the parse of the pattern.
#[derive(Clone)]
struct PatternChecker {
    pub pattern: Pattern,
    pub index: usize,
    pub start_position: usize,
}

impl PatternChecker {
    pub fn new(pattern: Pattern, start_position: usize) -> Self {
        return Self {
            pattern,
            index: 1,
            start_position,
        };
    }

    pub fn is_next_byte(&mut self, byte: u8) -> bool {
        if byte == self.pattern.bytes[self.index] {
            self.index += 1;
            return true;
        }
        return false;
    }

    pub fn is_finish(&self) -> bool {
        return self.index == self.pattern.bytes.len();
    }
}
