
pub fn vec_to_hexdump(os: &[u8]) -> String {
    let mut vs = Vec::new();

    for o in os.iter() {
        vs.push(format!("{:02X}", o));
    }
    return vs.join("");
}

