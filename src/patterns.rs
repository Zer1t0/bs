use std::fmt;
use crate::utils::vec_to_hexdump;

/// Struct to save a pattern
#[derive(Clone, Debug)]
pub struct Pattern {
    pub name: String,
    pub bytes: Vec<u8>,
}

impl Pattern {
    pub fn new(name: String, bytes: Vec<u8>) -> Self {
        return Self { name, bytes };
    }

    pub fn is_first_byte(&self, byte: u8) -> bool {
        return byte == self.bytes[0];
    }

    pub fn hexdump(&self) -> String {
        return vec_to_hexdump(&self.bytes);
    }
}

impl fmt::Display for Pattern {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.name.is_empty() {
            write!(f, "{}", vec_to_hexdump(&self.bytes))
        } else {
            write!(f, "{}", self.name)
        }
    }
}
