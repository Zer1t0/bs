<!-- cargo-sync-readme start -->

# bs

bs (binary searcher) is a little tool to search patterns in binary data.
It can be useful to locate binary signatures.

## Basic example

Search for PNG images in a tar file:
```shell
$ bs -H '89 50 4E 47 0D 0A 1A 0A' /tmp/pictures.tar
/tmp/pictures.tar:89504e470d0a1a0a -> 1187328 0x121e00
/tmp/pictures.tar:89504e470d0a1a0a -> 1208832 0x127200
/tmp/pictures.tar:89504e470d0a1a0a -> 1632256 0x18e800
/tmp/pictures.tar:89504e470d0a1a0a -> 1675776 0x199200
/tmp/pictures.tar:89504e470d0a1a0a -> 1758208 0x1ad400
/tmp/pictures.tar:89504e470d0a1a0a -> 1989632 0x1e5c00
/tmp/pictures.tar:89504e470d0a1a0a -> 2015232 0x1ec000
/tmp/pictures.tar:89504e470d0a1a0a -> 2021376 0x1ed800
```

You can also use several patterns, and give them names for identification:
```shell
$ bs -H JPG:ffd8,'PNG:89 50 4E 47 0D 0A 1A 0A' /tmp/pictures.tar
/tmp/pictures.tar:JPG(ffd8) -> 512 0x200
/tmp/pictures.tar:JPG(ffd8) -> 1011200 0xf6e00
/tmp/pictures.tar:PNG(89504e470d0a1a0a) -> 1187328 0x121e00
/tmp/pictures.tar:PNG(89504e470d0a1a0a) -> 1208832 0x127200
/tmp/pictures.tar:JPG(ffd8) -> 1298337 0x13cfa1
/tmp/pictures.tar:JPG(ffd8) -> 1529734 0x175786
/tmp/pictures.tar:JPG(ffd8) -> 1607598 0x1887ae
/tmp/pictures.tar:PNG(89504e470d0a1a0a) -> 1632256 0x18e800
/tmp/pictures.tar:PNG(89504e470d0a1a0a) -> 1675776 0x199200
/tmp/pictures.tar:JPG(ffd8) -> 1720191 0x1a3f7f
/tmp/pictures.tar:PNG(89504e470d0a1a0a) -> 1758208 0x1ad400
/tmp/pictures.tar:JPG(ffd8) -> 1765788 0x1af19c
/tmp/pictures.tar:JPG(ffd8) -> 1988753 0x1e5891
/tmp/pictures.tar:PNG(89504e470d0a1a0a) -> 1989632 0x1e5c00
/tmp/pictures.tar:PNG(89504e470d0a1a0a) -> 2015232 0x1ec000
/tmp/pictures.tar:PNG(89504e470d0a1a0a) -> 2021376 0x1ed800
/tmp/pictures.tar:JPG(ffd8) -> 2107560 0x2028a8
/tmp/pictures.tar:JPG(ffd8) -> 2125312 0x206e00
```

It seems that the JPG signature retrieves false positives. However since
the files are aligned you can search only in the positions aligned with 0x100:
```shell
$ bs -H JPG:ffd8,'PNG:89 50 4E 47 0D 0A 1A 0A' -a 0x100 /tmp/pictures.tar
/tmp/pictures.tar:JPG(ffd8) -> 512 0x200
/tmp/pictures.tar:JPG(ffd8) -> 1011200 0xf6e00
/tmp/pictures.tar:PNG(89504e470d0a1a0a) -> 1187328 0x121e00
/tmp/pictures.tar:PNG(89504e470d0a1a0a) -> 1208832 0x127200
/tmp/pictures.tar:PNG(89504e470d0a1a0a) -> 1632256 0x18e800
/tmp/pictures.tar:PNG(89504e470d0a1a0a) -> 1675776 0x199200
/tmp/pictures.tar:PNG(89504e470d0a1a0a) -> 1758208 0x1ad400
/tmp/pictures.tar:PNG(89504e470d0a1a0a) -> 1989632 0x1e5c00
/tmp/pictures.tar:PNG(89504e470d0a1a0a) -> 2015232 0x1ec000
/tmp/pictures.tar:PNG(89504e470d0a1a0a) -> 2021376 0x1ed800
/tmp/pictures.tar:JPG(ffd8) -> 2125312 0x206e00
```

Also, you can also use a file with the patterns:
```shell
$ bs- c patterns/pictures.toml -a 0x100 /tmp/pictures.tar
```
Besides, you can read from stdin:
```shell
$ cat /tmp/pictures.tar | bs -H 'PNG:89 50 4E 47 0D 0A 1A 0A'
```



<!-- cargo-sync-readme end -->
