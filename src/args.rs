use crate::patterns::Pattern;
use clap::{App, Arg, ArgGroup, ArgMatches};
use serde::Deserialize;
use std::collections::HashMap;
use std::fs;

fn args() -> App<'static, 'static> {
    App::new(env!("CARGO_PKG_NAME"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .version(env!("CARGO_PKG_VERSION"))
        .arg(
            Arg::with_name("file")
                .takes_value(true)
                .multiple(true)
                .help("File to search in"),
        )
        .arg(
            Arg::with_name("hexa")
                .long("hexa")
                .short("H")
                .takes_value(true)
                .use_delimiter(true)
                .help("Hexadecimal pattern")
                .validator(is_name_hexa_pattern),
        )
        .arg(
            Arg::with_name("conf")
                .long("conf")
                .short("c")
                .takes_value(true)
                .help("File with patterns")
                .validator(is_config_file_path),
        )
        .group(
            ArgGroup::with_name("patterns")
                .args(&["hexa", "conf"])
                .required(true)
                .multiple(true),
        )
        .arg(
            Arg::with_name("align")
                .long("align")
                .short("a")
                .takes_value(true)
                .help("To only search in alignment offsets")
                .validator(is_not_zero_usize)
                .default_value("1"),
        )
        .arg(
            Arg::with_name("verbosity")
                .short("v")
                .multiple(true)
                .help("Verbosity"),
        )
}

fn is_not_zero_usize(v: String) -> Result<(), String> {
    let size = str_to_usize(&v)?;

    if size == 0 {
        return Err("It cannot be 0".to_string());
    }
    return Ok(());
}

fn str_to_usize(v: &str) -> Result<usize, String> {
    let size;
    if v.starts_with("0x") {
        let no_prefix = v.trim_start_matches("0x");
        size = usize::from_str_radix(&no_prefix, 16);
    } else {
        size = usize::from_str_radix(&v, 10);
    }

    return size.map_err(|_| format!("Not valid integer '{}'", v));
}

pub fn is_config_file_path(v: String) -> Result<(), String> {
    return load_config_patterns(&v).map(|_| ());
}

pub fn is_name_hexa_pattern(v: String) -> Result<(), String> {
    let parts: Vec<String> = v.split(":").map(|s| s.to_string()).collect();
    return is_hexa_pattern(parts[parts.len() - 1].clone());
}

pub fn is_hexa_pattern(mut v: String) -> Result<(), String> {
    let hexa_chars = &[
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'A', 'b', 'B',
        'c', 'C', 'd', 'D', 'e', 'E', 'f', 'F',
    ];

    // remove spaces
    v.retain(|c| c != ' ');

    if v.is_empty() {
        return Err(format!("Empty hexa number"));
    }

    for c in v.chars() {
        if !hexa_chars.contains(&c) {
            return Err(format!("Invalid char '{}' in hexa number", c));
        }
    }
    return Ok(());
}

#[derive(Debug, Deserialize)]
pub struct Config {
    author: Option<String>,
    patterns: HashMap<String, ConfigPattern>,
}

#[derive(Debug, Deserialize)]
pub struct ConfigPattern {
    sign: String,
}

pub fn load_config(filepath: &str) -> Result<Config, String> {
    let content = fs::read_to_string(filepath)
        .map_err(|e| format!("Unable to read file '{}': {}", filepath, e))?;
    return toml::from_str(&content).map_err(|e| {
        format!("Error parsing configuration file '{}': {}", filepath, e)
    });
}

pub fn load_config_patterns(filepath: &str) -> Result<Vec<Pattern>, String> {
    let config = load_config(filepath)?;
    let mut patterns = Vec::new();

    for (name, pattern) in config.patterns.iter() {
        is_hexa_pattern(pattern.sign.clone())?;

        let hexa_bytes = hexa_string_to_bytes(&pattern.sign);
        patterns.push(Pattern::new(name.into(), hexa_bytes));
    }

    if patterns.is_empty() {
        return Err(format!("No patterns in {}", filepath));
    }
    return Ok(patterns);
}

pub struct Args {
    pub filepaths: Vec<String>,
    pub verbosity: usize,
    pub patterns: Vec<Pattern>,
    pub align: usize,
}

impl Args {
    pub fn parse_args() -> Self {
        let matches = args().get_matches();

        let mut patterns = Self::parse_hexas(&matches);

        if let Some(config_path) = matches.value_of("conf") {
            let mut config_patterns =
                load_config_patterns(config_path).unwrap();
            patterns.append(&mut config_patterns);
        }

        return Args {
            filepaths: Self::parse_files(&matches),
            verbosity: matches.occurrences_of("verbosity") as usize,
            align: str_to_usize(matches.value_of("align").unwrap()).unwrap(),
            patterns,
        };
    }

    fn parse_files(matches: &ArgMatches) -> Vec<String> {
        match matches.values_of("file") {
            None => Vec::new(),
            Some(files) => files.map(|s| s.into()).collect()
        }
    }


    fn parse_hexas(matches: &ArgMatches) -> Vec<Pattern> {
        if let Some(hexa_strs) = matches.values_of("hexa") {
            return hexa_strs.map(|hs| hexa_to_pattern(hs)).collect();
        }
        return Vec::new();
    }
}

fn hexa_to_pattern(hexa_name_str: &str) -> Pattern {
    let mut parts: Vec<&str> = hexa_name_str.split(":").collect();

    let hexa_str = parts.pop().unwrap();

    let hexa_bytes = hexa_string_to_bytes(hexa_str);

    let hexa_name = parts.join(":");
    return Pattern::new(hexa_name, hexa_bytes);
}

fn hexa_string_to_bytes(hexa_str: &str) -> Vec<u8> {
    let mut pattern = Vec::new();

    for numbers in hexa_str.split(" ") {
        if numbers.len() == 0 {
            continue;
        }

        let mut numbers = numbers.to_string();
        while !numbers.is_empty() {
            let mut n = numbers.remove(0).to_string();

            if !numbers.is_empty() {
                n = format!("{}{}", n, numbers.remove(0));
            }

            pattern
                .push(u8::from_str_radix(&n, 16).expect("Error parsing hexa"));
        }
    }

    return pattern;
}
